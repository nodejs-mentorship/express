const asyncHandler = require("express-async-handler");
const fs = require("fs");

const filePath = "src/students.json";

exports.students = asyncHandler(async (req, res) => {
  const students = readStudentsFromFile();
  res.send(students);
});

exports.student = asyncHandler(async (req, res) => {
  const students = readStudentsFromFile();
  const student = students.find((s) => s.id === parseInt(req.params.id));
  res.send(student);
});

exports.createStudent = asyncHandler(async (req, res) => {
  const students = readStudentsFromFile();
  students.push(req.body);
  writeStudentsToFile(students);
  res.send(students);
});

exports.updateStudent = asyncHandler(async (req, res) => {
  const students = readStudentsFromFile();
  students.map((s) => {
    if (s.id === parseInt(req.body.id)) {
      s.name = req.body.name;
      s.avrageGrade = req.body.avrageGrade;
    }
    return s;
  });
  writeStudentsToFile(students);
  const student = students.find((s) => s.id === parseInt(req.body.id));
  res.send(student);
});

exports.deleteStudent = asyncHandler(async (req, res) => {
  const students = readStudentsFromFile();
  const filteredStudents = students.filter(
    (s) => s.id !== parseInt(req.params.id)
  );
  writeStudentsToFile(filteredStudents);
  res.send(filteredStudents);
});

const readStudentsFromFile = () => {
  const studentsBuffer = fs.readFileSync(filePath);
  return JSON.parse(studentsBuffer.toString());
};

const writeStudentsToFile = (students) => {
  fs.writeFileSync(filePath, JSON.stringify(students));
};
