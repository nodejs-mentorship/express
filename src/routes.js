const express = require("express");
const {
  students,
  student,
  createStudent,
  updateStudent,
  deleteStudent,
} = require("./controllers/studentController");

const router = express.Router();

router.get("/student", students);
router.get("/student/:id", student);
router.post("/student", createStudent);
router.put("/student", updateStudent);
router.delete("/student/:id", deleteStudent);

module.exports = router;
